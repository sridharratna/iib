package com.wmb.filter;

import org.w3c.dom.NodeList;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;

public class FILTER_MF_JavaCompute extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");
		boolean updated = false;
		MbMessageAssembly outAssembly = null;
		try {
			MbMessage outMessage = new MbMessage(inAssembly.getMessage());
			outAssembly =  new MbMessageAssembly(inAssembly,outMessage);
			// ----------------------------------------------------------
			// Add user code below
			
			
			NodeList nodes = outMessage.getDOMDocument().getElementsByTagName("shipto");
			
			for(int i =0 ; i < nodes.getLength(); i++){
				if( nodes.item(0).getFirstChild().getFirstChild().getNodeValue().equalsIgnoreCase("sridevi")){
					nodes.item(0).getFirstChild().getFirstChild().setNodeValue("shivakumar");
					updated = true;
				}
			}
			
			
			

			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}

		if(updated)
			alt.propagate(outAssembly);
		else
			out.propagate(inAssembly);
	}

}
