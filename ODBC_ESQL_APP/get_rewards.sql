CREATE DEFINER=`root`@`localhost` PROCEDURE `get_rewards`(IN min_monthly_purchases TINYINT UNSIGNED
    , IN min_dollar_amount_purchased DECIMAL(10,2) UNSIGNED)
BEGIN

    CREATE TEMPORARY TABLE tmpCustomer (customer_id SMALLINT UNSIGNED NOT NULL PRIMARY KEY);

INSERT INTO tmpCustomer (customer_id)
SELECT p.customer_id
    FROM payment AS p
    GROUP BY customer_id
    HAVING SUM(p.amount) > min_dollar_amount_purchased
    AND COUNT(customer_id) > customer_id;
SELECT c.*
    FROM tmpCustomer AS t
    INNER JOIN customer AS c ON t.customer_id = c.customer_id;
    
        DROP TABLE tmpCustomer;

END