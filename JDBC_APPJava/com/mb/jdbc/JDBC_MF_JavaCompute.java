package com.mb.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;

public class JDBC_MF_JavaCompute extends MbJavaComputeNode {

	@SuppressWarnings("unchecked")
	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbMessage inMessage = inAssembly.getMessage();
		MbMessage outMessage = new MbMessage();
		MbMessageAssembly outAssembly = new MbMessageAssembly(inAssembly,outMessage);
		try {
			
			
			copyMessageHeaders(inMessage, outMessage);
			MbElement inRootElement = inMessage.getRootElement();
			List<MbElement>  idList =  (List<MbElement>) inRootElement.getLastChild().evaluateXPath("/Request/ID");
			int id = Integer.valueOf((String)idList.get(0).getValue());
			
			 //Class.forName("com.mysql.jdbc.Driver");  
			 //String url = "jdbc:mysql://localhost:3306/employees"; 
			 //Connection conn = DriverManager.getConnection(url,"root","root"); 
			Connection conn = getJDBCType4Connection("MYSQL_LOCAL", JDBC_TransactionType.MB_TRANSACTION_AUTO);
			 
			 Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);                                                 
			 ResultSet resultSet = stmt.executeQuery("SELECT * FROM EMPLOYEES WHERE EMP_NO ="+id);

			 MbElement root = outMessage.getRootElement();
			 MbElement xmlnsc = root.createElementAsLastChild("XMLNSC");
			 
			while(resultSet.next()){
				MbElement document = xmlnsc.createElementAsLastChild(MbElement.TYPE_NAME,"Employee",null);
				document.createElementAsLastChild(MbElement.TYPE_NAME_VALUE,"EMPID",resultSet.getString("EMP_NO"));
				document.createElementAsLastChild(MbElement.TYPE_NAME_VALUE,"FIRSTNAME",resultSet.getString("FIRST_NAME"));
				document.createElementAsLastChild(MbElement.TYPE_NAME_VALUE,"LASTNAME",resultSet.getString("LAST_NAME"));

			}
			out.propagate(outAssembly);
 
			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}

		finally {


			// clear the outMessage even if there's an exception
			outMessage.clearMessage();
			}
		// The following should only be changed
		// if not propagating message to the 'out' terminal

	}

	public void copyMessageHeaders(MbMessage inMessage, MbMessage outMessage)
			throws MbException {
		MbElement outRoot = outMessage.getRootElement();
		
		// iterate though the headers starting with the first child of the root
		// element
		MbElement header = inMessage.getRootElement().getFirstChild();
		while (header != null && header.getNextSibling() != null) // stop before
																	// the last
																	// child
																	// (body)
		{
			// copy the header and add it to the out message
			outRoot.addAsLastChild(header.copy());
			// move along to next header
			header = header.getNextSibling();
		}
	}

}
