

Importing file: C:\Users\sreer_000\Downloads\MessageModeling\resources\PURCHASES.cpy

Import options for COBOL: 
		CODEPAGE=ISO-8859-1
		ENDIAN=Little
		EXTENSION_CBL=FP
		EXTENSION_CCP=FP
		EXTENSION_COB=FP
		EXTENSION_CPY=DS
		EXT_DECIMAL_SIGN=ASCII
		FLOATING_POINT_FORMAT=0
		NSYMBOL=NATIONAL
		NUMPROC=PFD
		PLATFORM_SELECTION=0
		QUOTE=DOUBLE
		REMOTE_ENDIAN=Little
		TRUNC=STD
		SCHEMA_TARGET_NAMESPACE_URI=null
		CREATE_DEFAULT_VALUES_FROM_INITIAL_VALUES=true
		CREATE_FACETS_FROM_LEVEL_88_VALUE_CLAUSES=true
		CREATE_NULL_VALUES_FOR_FIELDS=true
		NULL_CHARACTER_FOR_STRING=%SP; 
		NULL_CHARACTER_FOR_NUMBER=%#r00; 
		STRING_PADDING_CHARACTER=%SP;
		NUMBER_PADDING_CHARACTER=0
		DEFAULT_FOR_STRING= 
		DEFAULT_FOR_NUMBER=0

Creating global element for type "PURCHASES"
Creating global element "PURCHASES" for type "PURCHASES"
Creating message "PURCHASES" from global element "PURCHASES"
Setting default value for elements of type Number  "0"
Setting default value for elements of type Number  "0"
Setting default value for elements of type Number  "0"
Setting default value for elements of type Number  "0"
Setting nillable property on all String elements and null value to "%SP; "
Setting nillable property on all Number elements and null value to "%#r00; "
Setting Padding Character for elements of type String  "%SP;"
Setting Padding Character for elements of type Number  "0"
Resolved: //PURCHASES;XSDComplexTypeDefinition/XSDParticle/XSDModelGroup/XSDParticle=14/PurchaseCount;XSDElementDeclaration
BIP0239W Byte alignment for elements referenced in the complex type "PURCHASES"  may not be correct. Byte alignment should be fixed by editing the file through the message definition editor. 
Probable cause: Element in the COBOL copy book has OCCURS DEPENDING ON clause.

Elapsed time processing this DFDL schema file: 0.891 seconds
Number of warnings for this DFDL schema file: 1


Number of files processed: 1


