package com.wmb.java;

import org.apache.commons.lang3.StringUtils;

public class WMBJava {
	
	public static Long addNumbers(Long a, Long b){
		return a+b;
	}

	
	public static String alignCenter(String str){
		return StringUtils.center(str, 50, '*');
	}
}
